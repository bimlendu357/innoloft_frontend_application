import { connect } from "react-redux";
import { compose } from "redux";
import {
  Switch,
  Route,
  BrowserRouter as Router,
  Redirect,
  withRouter,
} from "react-router-dom";
import { injectReducer, injectSaga } from "redux-injectors";
import reducer from "./reducer";
import saga from "./saga";
import Header from "components/Header";

const App = (props) => {
  return (
    <>
      {" "}
      <Header />
      <Switch>{/* <Route path={"*"} component={yet to make} /> */}</Switch>
    </>
  );
};

function mapStateToProps(state) {
  // all the props from redux
  return {};
}

function mapDispatchToProps(dispatch) {
  // connect all the function for disipacting action
  return {};
}

const withConnect = connect(mapStateToProps, mapDispatchToProps);

export default compose(
  injectReducer({ key: "app", reducer }),
  injectSaga({ key: "app", saga }),
  withConnect,
  withRouter
)(App);
